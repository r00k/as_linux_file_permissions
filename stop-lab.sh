#!/bin/bash

docker stop linux-fun-14

echo "Lab stopped. Removing lab."

docker rm linux-fun-14

echo "Lab container removed."

# as_linux_file_permissions

Lab built for the antisyphon linux file permissions webcast

Youtube: https://www.youtube.com/watch?v=D64O6aaKAKs  
Twitch: https://www.twitch.tv/videos/1529949901

## Getting started

All you need is docker for your operating system of choice installed.

Once that is done you can either clone this repo and use the `start-lab.sh` 
and `stop-lab.sh` scripts within, or you can use these raw docker commands:

Obtain and run the image
```
docker run -d -h linux-fun-14 --name linux-fun-14 registry.gitlab.com/r00k/as_linux_file_permissions:latest

docker exec -w /lab-files/ -u student -it linux-fun-14 /bin/bash
```

Once you're done being in the lab you can run `exit` from the shell. Then
you can stop and remove the lab image with the following commands:

```
docker stop linux-fun-14

docker rm linux-fun-14
```

If for whatever reason something gets messed up beyond your ability to
fix it, you can always just stop and remove the container, and then start
it all over again from a fresh state.

## The Lab

This lab takes place in the middle of the Linux Fundamentals class, so
some Linux knowledge is required, though I've modified the material to
mostly just rely on the information required in the web cast.

When you load into the lab you will start by looking at the `readme` file
in the same directory you load into (`/lab-files/`). That file will give
you the directions to start off with. Once you complete each objective
you will either receive new instructions directly to your shell, or you
will need to read the file you just gave yourself permissions to access.

When you complete all of the objectives you will receive a "YOU FINISHED"
message.

In addition to the commands covered in the web cast, the only commands you
will need are `cat` and `cd`. The `man` command may be useful if you need
to reference the help files for more information on various commands.

The challenges themselves will start with fairly prescriptive instructions,
and get more vague towards the end to let you problem solve with your new
tools.

## Is this representative of the rest of the Linux Fundamentals course?

Yes and no. The slides used in the webcast were a subset of the larger module
and are from an early pass of the class. Modules are being reworked in a 
quality pass to make the instruction more clear, slides easier to read and 
follow, and concepts to flow in a more progressive nature.

This lab is similar to the other labs being built for the class. They will all
be highly interactive and focus on developing real world administrative skills
and problem solving. This lab was rushed out the door, so quality and testing
have suffered a bit. If you encounter a problem, please let me know. There
are also several labs like this one per module, so this lab is just a taste.

The class is also accompanied by a wiki that has write ups for each module
(can we say "copy and pastable commands and reference?"), as well as detailed
instructions for each lab. Labs have objectives and hints so you can challenge
yourself to complete based on skill and problem solving, as well as step by
step instructions and explanations so you can check your work and see if you
missed anything. I'll often include optional solutions, best practices, and
other things to think about.

## I found a problem or I have feedback

Great! This is very much a work in progress and I rushed this lab out the door
to correspond with a last minute webcast. Feel free to reach out to me either on
Twitter (`@_r00k_`), or post an issue on the repo. I welcome all bugs and feedback
as I'll be able to refine the class while it's still in development. 

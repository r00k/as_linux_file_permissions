#!/bin/bash

setfacl -m u:maluser:rw /lab-files/.meta/lock-file

while true
do

student_tty=$(ps aux | grep student | grep bash | head -n 1 | awk '{ print $7 }')
swall="/dev/$student_tty"

if [ $(ls -l /lab-files/challenge1/file36 | cut -f4 -d" ") = "dev" ] && [ ! -f /tmp/progress/chall2 ]
then
cat /tmp/progress/challenge3 > $swall
touch /tmp/progress/chall2
fi

if [ $(find /tmp/challenge4/chall4 -perm -0400) ] && [ ! -f /tmp/challenge4/chall5 ]
then
touch /tmp/challenge4/chall5
chown student:student /tmp/challenge4/chall5
chmod 644 /tmp/challenge4/chall5
fi

if [ $(find /tmp/challenge4/chall5 -perm -0646 2>/dev/null) ] && [ ! -f /tmp/progress/chall4 ]
then
cat /tmp/progress/challenge5 > /tmp/challenge4/chall5
mkdir /tmp/challenge5
chown student:dev /tmp/challenge5
chmod 760 /tmp/challenge5
echo -e '\nBoom! Check the chall5 file for the next step\n' > $swall
touch /tmp/progress/chall4
fi

if [ $(find /tmp/challenge5 -perm -0070 2>/dev/null) ] && [ ! -f /tmp/progress/chall5 ]
then
echo -e '\nYou are really getting the hang of this!\nCheck the directory for nextsteps\n' > $swall
cat /tmp/progress/challenge6 > /tmp/challenge5/chall6
chown student:dev /tmp/challenge5/chall6
touch /tmp/progress/chall5
fi

if [ $(su maluser -c "find /lab-files/.meta/lock-file -writable") ]
then
su maluser -c "echo 'pwn3d n00bs' > /lab-files/.meta/lock-file"
elif [ ! -f /tmp/progress/chall6 ]
then
echo "process locked, automation normal" > /lab-files/.meta/lock-file
echo -e '\nYowza! That was a tough one, but you solved it. Almost there!\n\nFinal Challenge:\nSomewhere in /usr/local/challenge/ is a setuid script.\nUse the find command to find it.' > $swall
touch /tmp/progress/chall6
fi

sleep 2

done

#!/bin/bash

if [[ ! $(docker ps -a | grep linux-fun-14) ]]
then
    docker run -d -h linux-fun-14 --name linux-fun-14 registry.gitlab.com/r00k/as_linux_file_permissions:latest
fi

docker exec -w /lab-files/ -u student -it linux-fun-14 /bin/bash
